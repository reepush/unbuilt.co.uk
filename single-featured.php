<?php include 'config.php' ?>
<?php include 'partials/top.php' ?>

<body class="page-featured-project">
  <?php include 'partials/loader.php' ?>
  
  <div class="g-container">
    <?php include 'partials/header.php' ?>
    
    <div class="container">
      <div class="<?= 'content ' . (get_field('video') ? 'content-video' : 'content-images') ?>">
        <div class="slider">
          <div class="slider-links">
            <a class="prev"><svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" data-code="58132" data-tags="keyboard_arrow_left"><path d="M15.422 16.078l-1.406 1.406-6-6 6-6 1.406 1.407-4.594 4.594z" fill="#fff"/></svg></a>
            <a class="next"><svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" data-code="58377" data-tags="navigate_next"><path d="M9.984 6l6 6-6 6-1.406-1.406L13.172 12 8.578 7.406z" fill="#fff"/></svg></a>
            <a class="back" href="/featured/"><svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" data-code="59578" data-tags="settings_backup_restore"><path d="M12 3c4.97 0 9 4.03 9 9s-4.03 9-9 9c-2.063 0-3.984-.703-5.484-1.875l1.406-1.406c1.172.796 2.578 1.264 4.078 1.264 3.89 0 6.984-3.093 6.984-6.984S15.89 5.016 12 5.016 5.016 8.11 5.016 12h3l-4.032 3.984L0 12h3c0-4.97 4.03-9 9-9zm2.016 9c0 1.078-.938 2.016-2.016 2.016S9.984 13.078 9.984 12 10.922 9.984 12 9.984s2.016.938 2.016 2.016z" fill="#fff"/></svg></a>
          </div>
          
          <?php if (get_field('video')): ?>
            <div class="image-container media-loader"
                 data-type="video"
                 data-src="<?= str_replace('1080p', '540p', get_field('video')) ?>"
                 data-src-high="<?= get_field('video') ?>">
            </div>
          <?php else: ?>
            <?php foreach (get_field('images') as $image): ?>
              <div class="image-container media-loader"
                   data-type='background-image'
                   data-src="<?= $image['sizes']['large'] ?>"
                   data-src-high="<?= $image['url'] ?>">
              </div>
            <?php endforeach ?>
          <?php endif ?>
        </div>
      </div>
      
      <div class="sidebar">
        <div class="sidebar-content perfect-scrollbar">
          <h3><?= get_the_title() ?></h3>
          <?= get_field('content') ?>
        </div>
        
        <?php include 'partials/links.php' ?>
      </div>
    </div>
  </div>
</body>

<?php include 'partials/bottom.php' ?>
