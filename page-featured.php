<?php include 'config.php' ?>
<?php include 'partials/top.php' ?>

<?php
  $projects = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'featured'
  ));
  
  foreach ($projects as $project) {
    $images = get_field('images', $project->ID);
    $project->image = $images[0]['sizes']['large'];
  }
?>

<body class="page-featured">
  <?php include 'partials/loader.php' ?>
  
  <div class="g-container">
    <?php include 'partials/header.php' ?>
    
    <div class="container">
      <div class="content">
        <div class="gallery">
          <div class="column column-2">
            <?php foreach (array_slice($projects, 0, 2) as $project): ?>
              <a href="/featured/<?= $project->post_name ?>/"
                 class="media-loader"
                 data-src="<?= $project->image ?>"
                 title="<?= $project->post_title ?>">
              </a>
            <?php endforeach ?>
          </div>
          
          <div class="column column-3">
            <?php foreach (array_slice($projects, 2, 3) as $project): ?>
              <a href="/featured/<?= $project->post_name ?>/"
                 class="media-loader"
                 data-src="<?= $project->image ?>"
                 title="<?= $project->post_title ?>">
              </a>
            <?php endforeach ?>
          </div>
          
          <div class="column column-4">
            <?php foreach (array_slice($projects, 5, 4) as $project): ?>
              <a href="/featured/<?= $project->post_name ?>/"
                 class="media-loader"
                 data-src="<?= $project->image ?>"
                 title="<?= $project->post_title ?>">
              </a>
            <?php endforeach ?>
          </div>
          
          <div class="column column-3">
            <?php foreach (array_slice($projects, 9, 3) as $project): ?>
              <a href="/featured/<?= $project->post_name ?>/"
                 class="media-loader"
                 data-type="background-image"
                 data-src="<?= $project->image ?>"
                 title="<?= $project->post_title ?>">
              </a>
            <?php endforeach ?>
          </div>
        </div>
      </div>
      
      <div class="sidebar">
        <div class="sidebar-content perfect-scrollbar">
          <h3><?= get_field('sidebar_heading') ?></h3>
          <?= get_field('sidebar_content') ?>
        </div>
        
        <?php include 'partials/links.php' ?>
      </div>
    </div>
  </div>
</body>
  
<?php include 'partials/bottom.php' ?>
