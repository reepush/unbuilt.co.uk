<?php include 'config.php' ?>
<?php include 'partials/top.php' ?>

<body class="page-index">
  <?php include 'partials/loader.php' ?>
  
  <div class="g-container">
    <?php include 'partials/header.php' ?>
    
    <div class="container">
      <div class="content">
        <div class="media-loader"
             data-type="video"
             data-src="<?= str_replace('1080p', '540p', get_field('video')) ?>"
             data-src-high="<?= get_field('video') ?>">
        </div>
      </div>
      <div class="sidebar">
        <div class="sidebar-content perfect-scrollbar">
          <h3><?= get_field('sidebar_heading') ?></h3>
          <?= get_field('sidebar_content') ?>
        </div>
        
        <?php include 'partials/links.php' ?>
      </div>
    </div>
  </div>
</body>

<?php include 'partials/bottom.php' ?>
