/* <init> */
window._ = function(callback) {
  $(document).on('init', callback)
}
/* </init> */

/* <loader> */
$(function() {
  $('body').addClass('g-overflow-hidden')
  $('.loader img').attr('src', $('.loader img').attr('data-src') + '?' + Math.random().toString().slice(2))
  setTimeout(function() {
    $('body').removeClass('g-overflow-hidden')
    $('.loader').fadeOut()
  }, 4000)
})
/* </loader> */

/* <page-contact> */
_(function() {
  if (!$('body').hasClass('page-contact')) return

  $('.g-container form').on('submit', function(event) {
    event.preventDefault()
    
    // Add validation for Safari.
    var valid = true
    if ($('body').hasClass('browser-safari')) {
      $(this).find('[required]').each(function(index, field) {
        if (valid && $(field).val() == '') {
          valid = false
        }
      })
    }
    
    if (valid) {
      $('.form-submit .g-button-transparent').text('Thanks, we\'ll be in touch')
    }
  })

  var object_name1 = {"some_string1":"51.5275734","a_value":"30"};
  var object_name2 = {"some_string2":"-0.094204","a_value":"30"};
  var object_name3 = {"some_string3":{"url":window.THEME_PATH+'/images/page-contact-map-pointer.svg',"id":"373","height":"50","width":"38","thumbnail":window.THEME_PATH+"/images/page-contact-map-pointer.svg"},"a_value":"30"};
  var object_name4 = {"some_string4":"Our office - London","a_value":"30"};

  jQuery(".map-container").gmap3({
      action: "init",
      marker: {
          values: [ {
              latLng: [ object_name1.some_string1, object_name2.some_string2 ],
              data: object_name4.some_string4,
              options: {
               icon: object_name3.some_string3
              }
          } ],
          options: {
              draggable: false
          },
          events: {
              mouseover: function(a, b, c) {
                  var d = jQuery(this).gmap3("get"), e = jQuery(this).gmap3({
                      get: {
                          name: "infowindow"
                      }
                  });
                  if (e) {
                      e.open(d, a);
                      e.setContent(c.data);
                  } else jQuery(this).gmap3({
                      infowindow: {
                          anchor: a,
                          options: {
                              content: c.data
                          }
                      }
                  });
              },
              mouseout: function() {
                  var a = jQuery(this).gmap3({
                      get: {
                          name: "infowindow"
                      }
                  });
                  if (a) a.close();
              }
          }
      },
      map: {
          options: {
              zoom: 14,
              zoomControl: true,
              mapTypeControl: true,
              scaleControl: true,
              scrollwheel: false,
              streetViewControl: true,
              draggable: true,
              styles: [ {
                  featureType: "landscape",
                  stylers: [ {
                      saturation: -100
                  }, {
                      lightness: 65
                  }, {
                      visibility: "on"
                  } ]
              }, {
                  featureType: "poi",
                  stylers: [ {
                      saturation: -100
                  }, {
                      lightness: 51
                  }, {
                      visibility: "simplified"
                  } ]
              }, {
                  featureType: "road.highway",
                  stylers: [ {
                      saturation: -100
                  }, {
                      visibility: "simplified"
                  } ]
              }, {
                  featureType: "road.arterial",
                  stylers: [ {
                      saturation: -100
                  }, {
                      lightness: 30
                  }, {
                      visibility: "on"
                  } ]
              }, {
                  featureType: "road.local",
                  stylers: [ {
                      saturation: -100
                  }, {
                      lightness: 40
                  }, {
                      visibility: "on"
                  } ]
              }, {
                  featureType: "transit",
                  stylers: [ {
                      saturation: -100
                  }, {
                      visibility: "simplified"
                  } ]
              }, {
                  featureType: "administrative.province",
                  stylers: [ {
                      visibility: "off"
                  } ]
              }, {
                  featureType: "water",
                  elementType: "labels",
                  stylers: [ {
                      visibility: "on"
                  }, {
                      lightness: -25
                  }, {
                      saturation: -100
                  } ]
              }, {
                  featureType: "water",
                  elementType: "geometry",
                  stylers: [ {
                      hue: "#ffff00"
                  }, {
                      lightness: -25
                  }, {
                      saturation: -97
                  } ]
              } ]
          }
      }
  });
})
/* </page-contact> */

/* <page-featured-project> */
window.featuredProjectInterval = null
window.featuredProjectUserScrolled = false
_(function() {
  if (!$('body').hasClass('page-featured-project')) return

  $('.image-container').first().addClass('active')
  clearInterval(window.featuredProjectInterval)
  
  window.featuredProjectInterval = setInterval(function() {
    if (window.featuredProjectUserScrolled) {
      window.featuredProjectUserScrolled = false
      return
    }
    
    scroll('next')
  }, 3000)
  
  $('.slider-links .next').on('click', function() { scroll('next', true) })
  $('.slider-links .prev').on('click', function() { scroll('prev', true) })
  
  function scroll(direction, userScrolled) {
    window.featuredProjectUserScrolled = userScrolled
    
    var activeImageIndex = $('.image-container').index($('.image-container.active'))
    activeImageIndex = direction == 'next'
      ? activeImageIndex + 1 == $('.image-container').length ? 0 : activeImageIndex + 1
      : activeImageIndex == 0 ? $('.image-container').length - 1 : activeImageIndex - 1

    $('.image-container').removeClass('active').eq(activeImageIndex).addClass('active')
  }
})
/* </page-featured-project> */

/* <marquee> */
_(function() {
  $('.marquee').each(function(index, marquee) {
    if ($(marquee).find('.js-marquee').length || window.innerWidth < 1024) return

    $(marquee).marquee({
      duration: 10000,
      gap: 100,
      duplicated: true,
      pauseOnHover: true,
      startVisible: true,
    })
  })
})
/* </marquee> */

/* <pure-chat> */
window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } };
(function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '58ccf015-b149-4b35-8aff-b6efd3e0b71c', f: true }); done = true; } }; })();
/* </pure-chat> */

/* <popup> */
_(function() {
  window.popup = {
    show: function(popup) {
      if (window.innerWidth <= 820 && popup == 'how-we-can-help') {
        popup = 'get-started'
      }
      
      $('body').addClass('popup-opened')
      $('.popup').hide()
      $('.popup-' + popup).show()
      $('.popup__overlay').show()
    },

    hide: function() {
      $('body').removeClass('popup-opened')
      $('.popup').hide()
      $('.popup__overlay').hide()
    }
  }

  $('.popup__overlay').on('click', window.popup.hide)

  $('[data-popup-close]').on('click', function() {
    window.popup.hide()
    return false
  })

  $('[data-popup-open]').on('click', function() {
    var popup = $(this).data('popup-open')
    window.popup.show(popup)
    return false
  })
})
/* </popup> */

/* <popup-exit> */
window.exitPopupShown = false

// Track mouseout event on document
document.addEventListener("mouseout", function(e) {
  if (window.exitPopupShown || $('.loader').is(':visible')) return

  e = e ? e : window.event;

  // If this is an autocomplete element.
  if(e.target.tagName.toLowerCase() == "input")
    return;

  // Get the current viewport width.
  var vpWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  // If the current mouse X position is within 50px of the right edge
  // of the viewport, return.
  if(e.clientX >= (vpWidth - 50))
    return;

  // If the current mouse Y position is not within 50px of the top
  // edge of the viewport, return.
  if(e.clientY >= 50)
    return;

  // Reliable, works on mouse exiting window and
  // user switching active program
  var from = e.relatedTarget || e.toElement;
  if(!from) {
    window.popup.show('exit')
    window.exitPopupShown = true
  }
})
/* </popup-exit> */

/* <transition> */
$(function() {
  window.onpopstate = function(event) {
    $('body').attr('class', event.state.bodyClass)
    $('.header__navigation')[0].innerHTML = event.state.navigationContent
    $('.container')[0].innerHTML = event.state.containerContent

    $(document).trigger('init')
    $(document).scrollTop() > 100 && $(document).scrollTop(0)
  }

  $(document).trigger('init')
  $(document).on('click', 'a', function(event) {
    var href = $(this).attr('href')

    if (!href || event.metaKey || event.ctrlKey || href.match('mailto:')) return
    event.preventDefault()

    history.pushState({}, '', href)
    $.get(href).then(function(data) {
      var state = {
        bodyClass: data.match(/body class="(.*?)"/)[1],
        navigationContent: $(data).find('.header__navigation').html(),
        containerContent: $(data).find('.container').html(),
      }

      history.replaceState(state, data.match(/<title>(.*?)<\/title>/)[1], href)
      window.onpopstate({ state: state })
    })
  })
})
/* </transition> */

/* <forms> */
$(function() {
  $(document).on('submit', 'form', function(event) {
    event.preventDefault()
    
    // Add validation for Safari.
    var valid = true
    if ($('body').hasClass('browser-safari')) {
      $(this).find('[required]').each(function(index, field) {
        if (valid && $(field).val() == '') {
          valid = false
          window.alert('Please fill all required fields')
        }
      })
    }
    
    if (valid) {
      var $contactForm = $(this).parent().find('.contact-form form')
      $(this).find('[name]').each(function(index, field) {
        var name = $(field).attr('name')
        $contactForm.find('[name=' + name + ']').val($(field).val())
        $(field).val('')
      })
      
      $.post($contactForm.attr('action'), $contactForm.serialize())
      $(this).data('after-submit') && window.popup.show($(this).data('after-submit'))
    }
  })
})
/* </forms> */

/* <select> */
_(function() {
  $('.select select').on('change click', function(event) {
    $(this).parent().find('input').val(event.target.value)
  })
})
/* </select> */

/* <detection> */
_(function() {
  $('body').toggleClass('device-touch', 'ontouchstart' in document.documentElement)
  $('body').addClass('browser-' + Sniffr.browser.name)
})
/* </detection> */

/* <header> */
$(function() {
  $(document).on('click', '.header__menu-toggle', function(event) {
    $('.header__navigation').toggleClass('show')
    $('body').toggleClass('popup-opened')
  })
  
  $(document).on('click', '.header__navigation .header__link:not(.header__menu-toggle)', function(event) {
    if ($(event.target).closest('.header__menu-toggle').length) return
    $('.header__navigation').removeClass('show')
    $('body').toggleClass('popup-opened')
  })
})
/* </header> */

/* <media-loader> */
_(function() {
  $('.media-loader').each(function(index, container) {
    var type = $(container).data('type')
    var mediaUrl =
      (window.innerWidth >= 1600 || window.location.pathname == '/featured/the-vault/') &&
      $(container).data('src-high')
        ? $(container).data('src-high')
        : $(container).data('src')
    
    if (type != 'video') {
      mediaUrl = $(container).data('src')
      
      $(container).append(
        '<div class="media-loader__image-background" style="background-image: url(' + mediaUrl + ')"></div>' +
        '<div class="media-loader__loader"></div>'
      )
    } else {
      var video = $('<video autoplay="true" loop="true" class="media-loader__video" src="' + mediaUrl + '">')
      video[0].addEventListener('canplay', function() {
        $(container)
          .addClass('media-loader__loaded')
          .removeClass('media-loader__loading')
      })
      
      $(container).append(video)
      $(container).append('<div class="media-loader__loader"></div>')
    }
    
    setTimeout(function() {
      if (!$(container).hasClass('media-loader__loaded')) {
        $(container).addClass('media-loader__loading')
      }
    }, 200)
    
    if (type != 'video') {
      setTimeout(function() {
        $.get(mediaUrl, function() {
          setTimeout(function() {
            $(container)
              .addClass('media-loader__loaded')
              .removeClass('media-loader__loading')
          }, Math.random() * 250)
        })
      }, (Math.random() * 200) + 100)
    }
  })
})
/* </media-loader> */


/* <perfect-scrollbar> */
_(function() {
  $('.perfect-scrollbar').perfectScrollbar()
})
/* </perfect-scrollbar> */


/* <popup-get-started> */
$(function() {
  $('.popup-get-started [name="quote-contact"]').parent().find('select').on('change', function(event) {
    event.target.value == 'Telephone'
      ? $('.popup-get-started [name="quote-telephone"]').attr('required', '')
      : $('.popup-get-started [name="quote-telephone"]').removeAttr('required')
  })
})
/* </popup-get-started> */

