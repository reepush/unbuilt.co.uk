<?php include 'config.php' ?>
<?php include 'partials/top.php' ?>

<?php
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'blog'
  ));
?>

<body class="page-blog">
  <?php include 'partials/loader.php' ?>
  
  <div class="g-container">
    <?php include 'partials/header.php' ?>
    
    <div class="container">
      <div class="content">
        <div class="content-scroll perfect-scrollbar">
          <h1><?= get_the_title() ?></h1>
          <?= get_field('content') ?>
        </div>
      </div>
      
      <div class="sidebar">
        <div class="sidebar-content perfect-scrollbar">
          <?php foreach ($posts as $post): ?>
            <a class="blog-post" href="/blog/<?= $post->post_name ?>/">
              <div class="blog-post-image"
                   style="background-image: url(<?= get_field('image', $post->ID)['sizes']['thumbnail'] ?>)">
              </div>
              <h5><?= $post->post_title ?></h5>
            </a>
          <?php endforeach ?>
        </div>
        
        <?php include 'partials/links.php' ?>
      </div>
    </div>
  </div>
</body>

<?php include 'partials/bottom.php' ?>
