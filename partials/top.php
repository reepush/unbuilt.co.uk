<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?= $THEME_PATH ?>/images/favicon.png">
  <title>Unbuilt – Visualising Interiors</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?= $THEME_PATH . '/style.css?' . md5_file($DIRECTORY . '/style.css') ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/css/perfect-scrollbar.min.css">
  <?php include 'wp-styles.css' ?>
</head>

