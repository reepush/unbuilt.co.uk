  <?php
    echo '<div class="popup__overlay"></div>';
    include 'popups/popup-get-started.php';
    include 'popups/popup-get-started-thanks.php';
    include 'popups/popup-how-we-can-help.php';
    include 'popups/popup-more-info.php';
    include 'popups/popup-more-info-thanks.php';
    include 'popups/popup-cutouts.php';
    include 'popups/popup-cutouts-thanks.php';
    include 'popups/popup-exit.php';
    include 'popups/popup-exit-thanks.php';
  ?>

  <script>window.THEME_PATH = '<?= $THEME_PATH ?>'</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDI78IM3c_bnEixa29mmkPscwlOYP62zao"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.Marquee/1.3.94/jquery.marquee.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gmap3/6.0.0/gmap3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.8.1/js/perfect-scrollbar.jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sniffr@1.1.4/src/sniffr.min.js"></script>
  <script src="<?= $THEME_PATH . '/script.js?' . md5_file($DIRECTORY . '/script.js') ?>"></script>

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109039071-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109039071-1');
  </script>
</html>
