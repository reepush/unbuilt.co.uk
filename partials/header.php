<div class="header">
  <a href="/" class="header__logo">
    <img src="<?= $THEME_PATH ?>/images/logo.png">
  </a>
  
  <div class="header__container">
    <div class="header__top">
      <a href="/" class="header__company-name">Unbuilt</a>
      
      <a class="header__menu-toggle header__link">Menu</a>
      <div class="header__navigation">
        <a class="header__menu-toggle">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
        </a>
        
        <a href="/" class="header__link header__link-page-index">Home</a>
        <a href="/about/" class="header__link header__link-page-about">About</a>
        <a href="/featured/" class="header__link header__link-page-featured">Featured</a>
        <a href="/extended/" class="header__link header__link-page-extended">Extended</a>
        <a href="/blog/" class="header__link header__link-page-blog">Blog</a>
        <!-- <a href="/faq/" class="header__link header__link-page-faq">FAQ</a> -->
        <a href="/contact/" class="header__link header__link-page-contact">Contact</a>
      </div>
    </div>
    
    <div class="header__bottom">
      <div class="header__caption">Visualising Interiors</div>
      <div class="header__banner marquee" data-popup-open="get-started">
        <span>381 clients — 3,212 projects completed — 34,137 hours of experience — 87 team members — click to start your project today</span>
        <span>click to start your project today</span>
        <span>Visualising Interiors</span>
      </div>
    </div>
  </div>
</div>
