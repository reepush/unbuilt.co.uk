<div class="popup popup-get-started-thanks">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup__content popup-get-started-thanks__content">
    <h1>
      Thank you for your<br>submission.<br><br>
      We'll get back to you<br>shortly.
    </h1>
  </div>
</div>