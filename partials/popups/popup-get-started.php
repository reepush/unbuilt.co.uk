<div class="popup popup-get-started">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup__content popup-get-started__content">
    <h1>Quote</h1>
    
    <form data-after-submit="get-started-thanks">
      <div class="popup-get-started__form-control">
        <input placeholder="Name" name="quote-name" required>
      </div>
      
      <div class="popup-get-started__form-control">
        <input type="email" placeholder="Email" name="quote-email" required>
      </div>
      
      <div class="popup-get-started__form-row">
        <div class="popup-get-started__form-control popup-get-started__one-third">
          <div class="select">
            <input placeholder="Request" name="quote-type" tabindex="-1" required>
            <select>
              <option>Visuals</option>
              <option>Animation</option>
              <option>3D modelling</option>
              <option>Other</option>
            </select>
          </div>
        </div>
        
        <div class="popup-get-started__form-control popup-get-started__one-third">
          <div class="select">
            <input placeholder="Number of images" name="quote-images" tabindex="-1" required>
            <select>
              <option>1-3</option>
              <option>3+</option>
              <option>Not sure yet</option>
              <option>N/A</option>
            </select>
          </div>
        </div>
        
        <div class="popup-get-started__form-control popup-get-started__one-third">
          <div class="select">
            <input placeholder="How shall we contact you?" name="quote-contact" tabindex="-1" required>
            <select>
              <option>Telephone</option>
              <option>Email</option>
              <option>Either</option>
            </select>
          </div>
        </div>
      </div>
      
      <div class="popup-get-started__form-row">
        <div class="popup-get-started__form-control popup-get-started__one-third">
          <div class="select">
            <input placeholder="3D model" name="quote-model" tabindex="-1" required>
            <select>
              <option>Exists</option>
              <option>Doesn’t exist</option>
              <option>N/A</option>
            </select>
          </div>
        </div>
        
        <div class="popup-get-started__form-control popup-get-started__one-third">
          <input placeholder="Deadline" name="quote-deadline" required>
        </div>
        
        <div class="popup-get-started__form-control popup-get-started__one-third">
          <input type="tel" placeholder="Telephone?" name="quote-telephone">
        </div>
      </div>
      
      <div class="popup-get-started__form-bottom">
        <img class="popup-get-started__logo" src="<?= $THEME_PATH ?>/images/logo.png">
        
        <div class="popup-get-started__form-row">
          <textarea placeholder="Brief overview" name="quote-overview" required></textarea>
        </div>
        
        <div class="popup-get-started__form-submit">
          <button class="g-button-rounded">Submit</button>
          <p>
            <span>First job completely free if you're not satisfied</span><br>
            <small>(T's and C's to follow)</small>
          </p>
        </div>
      </div>
    </form>
    
    <div class="contact-form g-hidden">
      <?= do_shortcode('[contact-form-7 id="118" title="Quote"]') ?>
    </div>
  </div>
</div>