<div class="popup popup-cutouts-thanks">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup__content popup-cutouts-thanks__content">
    <h1>
      Great, please check your inbox.<br>
      <span class="popup-cutouts-thanks__title-bottom">This months download link<br> has been emailed to you.</span>
    </h1>
    
    <div class="popup-cutouts-thanks__bottom">
      <div class="popup-cutouts-thanks__bottom-caption">
        We look forward to helping you build your collection!
      </div>
      
      <div class="popup-cutouts-thanks__logo-container">
        <img class="popup-cutouts-thanks__logo" src="<?= $THEME_PATH ?>/images/logo-inverted.png">
      </div>
    </div>
  </div>
</div>