<div class="popup popup-cutouts">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup__content popup-cutouts__content">
    <h1>
      50 <strong>free</strong> photoshop cutouts<br>
      <span class="popup-cutouts__title-upper">every month</span>
    </h1>
    
    <div class="popup-cutouts__bottom">
      <div class="popup-cutouts__bottom-caption">
        600 free cutouts per year
      </div>
      
      <div class="popup-cutouts__bottom-form-container">
        <form class="popup-cutouts__bottom-form" data-after-submit="cutouts-thanks">
          <input type="email" placeholder="Email" name="cutouts-email" required></input>
          <button class="g-button-rounded">Submit</button>
        </form>
        
        <div class="contact-form g-hidden">
          <?= do_shortcode('[contact-form-7 id="155" title="Cutouts"]') ?>
        </div>
        
        <div class="popup-cutouts__bottom-form-caption">
          By providing us with your email address, you authorise Unbuilt to add you to our newsletter mailing list
        </div>
      </div>
      
      <div class="popup-cutouts__bottom-caption">
        Cloud based as well as downloadable
      </div>
      
      <div class="popup-cutouts__logo-container">
        <img class="popup-cutouts__logo" src="<?= $THEME_PATH ?>/images/logo-inverted.png">
      </div>
    </div>
  </div>
</div>