<div class="popup popup-exit">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup-exit__caption">Jon Medlock<br>Founder</div>
  <img class="popup-exit__logo" src="<?= $THEME_PATH ?>/images/logo-inverted.png">
  
  <div class="popup__content popup-exit__content">
    <h1>Before you leave...</h1>
    <p>
      Apprehensive about hiring a visualisation company?<br> You're not alone!<br><br>
      
      We've put together our...
      <strong>'Top 10 tips for outsourcing visualisations'</strong>
      full of invaluable advice and guidance that will ensure<br>
      you don't make the same mistakes countless other design<br>
      firms continue to.
    </p>
    
    <img class="popup-exit__arrow" src="<?= $THEME_PATH ?>/images/popup-exit-arrow.png">
    
    <form class="popup-exit__form" data-after-submit="exit-thanks">
      <input type="email" name="exit-email" required placeholder="Your email">
      <p>
        By providing us with your email address, you authorise<br>
        Unbuilt to add you to our newsletter mailing list.
      </p>
    </form>
    
    <div class="contact-form g-hidden">
      <?= do_shortcode('[contact-form-7 id="157" title="Exit"]') ?>
    </div>
  </div>
</div>