<div class="popup popup-exit-thanks">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup-exit-thanks__caption">Jon Medlock<br>Founder</div>
  <img class="popup-exit-thanks__logo" src="<?= $THEME_PATH ?>/images/logo-inverted.png">
  
  <div class="popup-thanks__content popup-exit-thanks__content">
    <h1>
      <span>Done</span>
      <img src="<?= $THEME_PATH ?>/images/popup-exit-thanks-check.png">
    </h1>
    <h2>Check your inbox shortly...</h2>
    
    <p>
      <img src="<?= $THEME_PATH ?>/images/popup-exit-thanks-envelope.png">
      
      Our comprehensive <strong>‘Top 10 tips for outsourcing visualisations’</strong><br>
      guide has been emailed to you and should be with you soon.<br><br>

      If there’s anything at all we can help with in the meantime please<br>
      be sure to drop us a message, we’d love to hear from you.
    </p>
  </div>
</div>