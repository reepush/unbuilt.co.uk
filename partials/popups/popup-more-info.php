<div class="popup popup-more-info">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup__content popup-more-info__content">
    <h1>More info</h1>
    <p>I’d prefer to learn more about Unbilt<br>before proceeding any further, please...</p>
    
    <form class="popup-more-info__form" data-after-submit="more-info-thanks">
      <input type="email" name="more-info-email" placeholder="Email" required>
      
      <div class="popup-more-info__form-submit">
        <button class="g-button-rounded">Submit</button>
      </div>
    </form>
    
    <div class="contact-form g-hidden">
      <?= do_shortcode('[contact-form-7 id="151" title="More info"]') ?>
    </div>
  </div>
</div>