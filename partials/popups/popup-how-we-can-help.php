<div class="popup popup-how-we-can-help">
  <a class="popup__close" data-popup-close>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" data-code="58829" data-tags="close"><path d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z" fill="#2d2d2d"/></svg>
  </a>
  
  <div class="popup__content popup-how-we-can-help__content">
    <h1>How can we help?</h1>
    
    <div class="popup-how-we-can-help__links">
      <a class="popup-how-we-can-help__link" data-popup-open="get-started">
        <span class="popup-how-we-can-help__link-title">Quote</span>
        <span class="popup-how-we-can-help__link-caption">First job completely free if<br>you are not satisfied</span>
      </a>
      
      <a class="popup-how-we-can-help__link" data-popup-open="more-info">
        <span class="popup-how-we-can-help__link-title">More info</span>
        <span class="popup-how-we-can-help__link-caption">Prefer to find out more about the<br>company and process first?</span>
      </a>
    </div>
  </div>
</div>