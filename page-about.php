<?php include 'config.php' ?>
<?php include 'partials/top.php' ?>

<body class="page-about">
  <?php include 'partials/loader.php' ?>
  
  <div class="g-container">
    <?php include 'partials/header.php' ?>
    
    <div class="container">
      <div class="container-left">
        
        <div class="container-left-top">
          <div class="sidebar-left">
            <div class="sidebar-content perfect-scrollbar">
              <h3><?= get_field('sidebar_heading') ?></h3>
              <?= get_field('sidebar_content') ?>
            </div>
          </div>
          
          <div class="content">
            <div class="feature feature-visualisation">
              <img class="feature-icon" src="<?= $THEME_PATH ?>/images/page-about-visualisations-icon.png">
              <?= get_field('visualisations_content') ?>
            </div>
            
            <div class="feature feature-right feature-animation">
              <img class="feature-icon" src="<?= $THEME_PATH ?>/images/page-about-animation-icon.png">
              <?= get_field('animation_content') ?>
            </div>
          </div>
        </div>
        
        <div class="container-left-bottom">
          <div class="clients-banner marquee">
            <div class="clients-banner-content">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-1.png">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-2.png">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-3.png">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-4.png">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-5.png">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-6.png">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-7.png">
              <img src="<?= $THEME_PATH ?>/images/page-about-logo-8.png">
            </div>
          </div>
        </div>
      </div>
      
      <div class="container-right">
        <div class="sidebar-right">
          <div class="sidebar-content">
            <div class="feature feature-modelling">
              <img class="feature-icon" src="<?= $THEME_PATH ?>/images/page-about-modeling-icon.png">
              <?= get_field('modelling_content') ?>
            </div>
          </div>
          
          <?php include 'partials/links.php' ?>
        </div>
      </div>
    </div>
  </div>
</body>

<?php include 'partials/bottom.php' ?>
