<?php include 'config.php' ?>
<?php include 'partials/top.php' ?>

<body class="page-contact">
  <?php include 'partials/loader.php' ?>
  
  <div class="g-container">
    <?php include 'partials/header.php' ?>
    
    <div class="container">
      <div class="content">
        <?= get_field('content') ?>
        
        <form class="form">
          <div class="g-form-row">
            <label class="g-form-control g-form-control-half">
              <div class="g-form-label">Name</div>
              <input type="text" name="contact-name" required>
            </label>
            
            <label class="g-form-control g-form-control-half">
              <div class="g-form-label">Email</div>
              <input type="email" name="contact-email" required>
            </label>
          </div>
          
          <label class="g-form-control">
            <div class="g-form-label">Message</div>
            <textarea name="contact-message" required></textarea>
          </label>
          
          <div class="form-submit">
            <button class="g-button-transparent">Submit</button>
          </div>
        </form>
        
        <div class="contact-form g-hidden">
          <?= do_shortcode('[contact-form-7 id="161" title="Contact"]') ?>
        </div>
      </div>
      
      <div class="sidebar">
        <div class="sidebar-content">
          <div class="map-container"></div>
        </div>
        
        <?php include 'partials/links.php' ?>
      </div>
    </div>
  </div>
</body>

<?php include 'partials/bottom.php' ?>
