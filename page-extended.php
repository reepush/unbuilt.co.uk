<?php include 'config.php' ?>
<?php include 'partials/top.php' ?>

<?php
  $projects = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'extended'
  ));
  
  foreach ($projects as $project) {
    $project->image = get_field('image', $project->ID)['sizes']['thumbnail'];
  }
?>

<body class="page-extended">
  <?php include 'partials/loader.php' ?>
  
  <div class="g-container">
    <?php include 'partials/header.php' ?>
    
    <div class="container">
      <div class="content">
        <div class="gallery">
          <?php foreach ($projects as $project): ?>
            <a class="item media-loader"
               href="/extended/<?= $project->post_name ?>/"
               title="<?= $project->post_title ?>"
               data-type="background-image"
               data-src="<?= $project->image ?>">
            </a>
          <?php endforeach ?>
        </div>
      </div>
      
      <div class="sidebar">
        <div class="sidebar-content perfect-scrollbar">
          <h3><?= get_field('sidebar_heading') ?></h3>
          <?= get_field('sidebar_content') ?>
        </div>
        
        <?php include 'partials/links.php' ?>
      </div>
    </div>
  </div>
</body>

<?php include 'partials/bottom.php' ?>
